package utfpr.ct.dainf.pratica;

import java.util.ArrayList;
import java.util.List;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <T> Tipo do ponto
 */
public class Poligonal<T extends Ponto2D> {
    private final List<T> vertices = new ArrayList<>();
        public int getN(){
            return vertices.size();
        }
        
        public int get(int i) throws Exception{
            if(i <= vertices.size()){ 
               return vertices.indexOf(i);
            }
            else{
                throw new Exception(
                "get(" + i + "%d): índice inválido");
            }
        }
        
        
        public void set(int i, T p){
            vertices.set(i, p);
        }
        
        public double getComprimento(){
            int ini = vertices.indexOf(0);
            int fim = vertices.size()-1;
            if (ini != fim){
                if(ini > fim)
                return fim-ini;
                else {
                    return ini-fim;
                }
            }
            else return 0;
        }
        
}
